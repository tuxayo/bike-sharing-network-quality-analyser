import pandas as pd


def filter_by_station_index(df, station_index):
    return df[df['index'] == station_index]


# STATS ONE STATION
def make_history_ratio_time_no_empty_slots(df):
    df = df[['empty_slots', 'timestamp']]
    return df.groupby(df['timestamp'].dt.date).apply(ratio_time_no_empty_slots)


def make_history_ratio_time_no_bikes(df):
    df = df[['free_bikes', 'timestamp']]
    return df.groupby(df['timestamp'].dt.date).apply(ratio_time_no_bikes)


def make_history_ratio_time_no_bikes_2(df):
    """
    Doesn't preserve the timestamp.
    Maybe faster but less simple
    """
    history_by_day = df.groupby(df['timestamp'].dt.day)
    return pd.Series([ratio_time_no_bikes(data)
                      for day, data in history_by_day])


def ratio_time_no_bikes(df):
    return _ratio_of_time_column_have_value(df, 'free_bikes', 0)


def ratio_time_no_empty_slots(df):
    return _ratio_of_time_column_have_value(df, 'empty_slots', 0)


def _ratio_of_time_column_have_value(df, column, value):
    time_equal_to_value = df['timestamp'].diff().shift(-1)[df[column] == value].sum()
    # TODO check if it will be a bottleneck because it's ineficient (can be
    # replaced by substracting two dates)
    time_total = df['timestamp'].diff().shift(-1).sum()
    return time_equal_to_value/time_total


# STATS WHOLE NETWORK

def make_network_probability_no_bikes_history(df):
    """
    Probability no bikes means that if you take a random station, what was the
    probability for it to be empty? For each day.

    :returns: Series with network probability no bikes history

    """
    return _make_network_probability_issue_history(df, make_history_ratio_time_no_bikes)


def make_network_probability_full_history(df):
    """
    Probability full means that if you take a random station, what was the
    probability for it to be full? For each day.

    :returns: Series with network probability no bikes history

    """
    return _make_network_probability_issue_history(df, make_history_ratio_time_no_empty_slots)


def _make_network_probability_issue_history(df, make_issue_history_func):
    list_stations_no_bikes_history = _make_all_stations_probability_issue_history(df, make_issue_history_func)
    # maybe do this instead https://stackoverflow.com/a/26110278/3682839
    df_stations_no_bikes_history = pd.DataFrame(list_stations_no_bikes_history)
    return df_stations_no_bikes_history.mean()


def _make_all_stations_probability_issue_history(df, make_issue_history_func):
    """
    :returns: DataFrame with all the stations probability history.
    With days on one axe and stations on the other.
    """
    print('computing: _make_all_stations_probability_issue_history()')
    stations = df['index'].unique()
    all_stations_issue_history = []
    for station_index in stations:
        print('.', end='')
        station_raw_history = filter_by_station_index(df, station_index)
        station_history_with_issue = make_issue_history_func(station_raw_history)
        all_stations_issue_history.append(station_history_with_issue)
    return all_stations_issue_history
