import os


_this_module_dir = os.path.dirname(__file__)
DATA_FOLDER = os.path.join(_this_module_dir, '..', 'data')
